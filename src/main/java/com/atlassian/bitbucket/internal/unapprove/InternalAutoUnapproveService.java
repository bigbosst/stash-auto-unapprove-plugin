package com.atlassian.bitbucket.internal.unapprove;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;

import javax.annotation.Nonnull;

public interface InternalAutoUnapproveService extends AutoUnapproveService {

    void cleanup(@Nonnull Repository repository);

    void withdrawApprovals(@Nonnull PullRequest pullRequest);
}
