package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.repository.Repository;

import javax.annotation.Nonnull;

public interface AutoUnapproveService {

    boolean isEnabled(@Nonnull PullRequest pullRequest);

    boolean isEnabled(@Nonnull Repository repository);

    void setEnabled(@Nonnull Repository repository, boolean enabled);
}
