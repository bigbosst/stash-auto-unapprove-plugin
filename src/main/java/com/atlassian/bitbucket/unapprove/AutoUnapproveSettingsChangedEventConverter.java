package com.atlassian.bitbucket.unapprove;

import com.atlassian.bitbucket.audit.AuditEntry;
import com.atlassian.bitbucket.audit.AuditEntryBuilder;
import com.atlassian.bitbucket.audit.AuditEntryConverter;
import com.atlassian.bitbucket.util.AuditUtils;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Converts an {@link AutoUnapproveSettingsChangedEvent} to an {@link AuditEntry} for inclusion in the audit log.
 * <p>
 * This class appears in the API because it is attached to an annotation on {@link AutoUnapproveSettingsChangedEvent}.
 * It is <i>not</i> considered part of the API, however, and offers no compatibility guarantees. Plugin developers
 * should not use this class.
 */
public class AutoUnapproveSettingsChangedEventConverter implements AuditEntryConverter<AutoUnapproveSettingsChangedEvent> {

    @Nonnull
    @Override
    public AuditEntry convert(@Nonnull AutoUnapproveSettingsChangedEvent event, AuditEntryBuilder builder) {
        ImmutableMap<String, Object> details = ImmutableMap.of("enabled", event.isEnabled());

        String detailsJson;
        try {
            detailsJson = AuditUtils.toJSONString(details);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to convert map %s to JSON", details), e);
        }

        return builder.action(event.getClass())
                .details(detailsJson)
                .repository(event.getRepository())
                .target(AuditUtils.toProjectAndRepositoryString(event.getRepository()))
                .timestamp(event.getDate())
                .user(event.getUser())
                .build();
    }
}
