package com.atlassian.bitbucket.internal.unapprove.ui;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.ui.ValidationErrors;
import com.atlassian.bitbucket.unapprove.AutoUnapproveService;
import com.atlassian.bitbucket.view.TemplateRenderingException;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.bitbucket.internal.unapprove.ui.AutoUnapproveFormFragment.*;
import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AutoUnapproveFormFragmentTest {

    @Mock
    private Appendable appendable;
    private Map<String, Object> context;
    @InjectMocks
    private AutoUnapproveFormFragment fragment;
    @Mock
    private SoyTemplateRenderer templateRenderer;
    @Mock
    private Repository repository;
    @Mock
    private AutoUnapproveService unapproveService;

    @Before
    public void setup() {
        context = ImmutableMap.of("repository", repository);
    }

    @Test
    public void testDoError() throws IOException {
        Map<String, Object> context = new HashMap<>();
        Map<String, Collection<String>> fieldErrors = emptyMap();

        fragment.doError(appendable, buildParams("true"), fieldErrors, context);
        assertThat(context, allOf(
                Matchers.<String, Object>hasEntry(FIELD_KEY, Boolean.TRUE),
                Matchers.hasEntry(equalTo(FIELD_ERRORS), sameInstance(fieldErrors))));

        verify(templateRenderer).render(same(appendable), eq(TEMPLATE_MODULE_KEY), eq(FRAGMENT_TEMPLATE), same(context));
    }

    @Test
    public void testDoView() throws IOException {
        Map<String, Object> context = new HashMap<>(this.context); // Must be mutable

        when(unapproveService.isEnabled(same(repository))).thenReturn(true);

        fragment.doView(appendable, context);
        assertThat(context, Matchers.hasEntry(FIELD_KEY, Boolean.TRUE));

        verify(templateRenderer).render(same(appendable), eq(TEMPLATE_MODULE_KEY), eq(FRAGMENT_TEMPLATE), same(context));
        verify(unapproveService).isEnabled(same(repository));
    }

    @Test(expected = TemplateRenderingException.class)
    public void testDoViewThrowsWhenRenderFails() throws IOException {
        Map<String, Object> context = new HashMap<>(); // Must be mutable

        doThrow(SoyException.class)
                .when(templateRenderer)
                .render(same(appendable), eq(TEMPLATE_MODULE_KEY), eq(FRAGMENT_TEMPLATE), same(context));

        fragment.doView(appendable, context);
        assertThat(context, Matchers.hasEntry(FIELD_KEY, Boolean.FALSE));

        verify(templateRenderer).render(same(appendable), eq(TEMPLATE_MODULE_KEY), eq(FRAGMENT_TEMPLATE), same(context));
        verify(unapproveService).isEnabled(same(repository));
    }

    @Test
    public void testExecute() {
        fragment.execute(buildParams("true"), context);

        verify(unapproveService).setEnabled(same(repository), eq(true));
    }

    @Test
    public void testExecuteWithEmptyValues() {
        fragment.execute(buildParams(null), context);

        verify(unapproveService).setEnabled(same(repository), eq(false));
    }

    @Test
    public void testExecuteWithoutSetting() {
        fragment.execute(emptyMap(), context);

        verify(unapproveService).setEnabled(same(repository), eq(false));
    }

    @Test
    public void testValidate() {
        fragment.validate(emptyMap(), mock(ValidationErrors.class), emptyMap());
    }

    private static Map<String, String[]> buildParams(String value) {
        String[] values = value == null ? new String[0] : new String[]{value};

        return ImmutableMap.of(FIELD_KEY, values);
    }
}
