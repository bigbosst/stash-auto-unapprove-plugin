package com.atlassian.bitbucket.internal.unapprove;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ImportResource("classpath:git-test-context.xml")
@PropertySource({"classpath:application-internal.properties", "classpath:application-default.properties"})
public class GitTestConfig {
}
