package it.com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.WebDriverCheckboxElement;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import it.com.atlassian.bitbucket.internal.unapprove.rest.fragment.AutoUnapproveRestFragmentTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1;
import static com.atlassian.bitbucket.test.DefaultFuncTestData.getProject1Repository1;
import static it.com.atlassian.bitbucket.internal.unapprove.rest.fragment.AutoUnapproveRestFragmentTest.assertEnabled;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AutoUnapproveBrowserTest extends BaseRetryingFuncTest {

    protected static final BitbucketTestedProduct BITBUCKET = TestedProductFactory.create(BitbucketTestedProduct.class);

    @After
    public void cleanup() {
        AutoUnapproveRestFragmentTest.setEnabled(getProject1(), getProject1Repository1(), false);
    }

    @Before
    public void setup() {
        BITBUCKET.visit(BitbucketLoginPage.class).login(getAdminUser(), getAdminPassword(),
                PullRequestRepositorySettingsPage.class, getProject1(), getProject1Repository1());
    }

    @Test
    public void testAddSuggestedReviewer() {
        // Make sure its disabled to start with
        assertEnabled(getProject1(), getProject1Repository1(), false);
        PullRequestRepositorySettingsPage page = visitPullRequestCreatePage();
        WebDriverCheckboxElement checkbox = page.getAutoUnapproveCheckbox();
        assertFalse(checkbox.isChecked());

        // Visit the page and enable it
        checkbox.check();
        page.save();

        // Revisit the page and ensure its still enabled
        page = visitPullRequestCreatePage();
        assertTrue(page.getAutoUnapproveCheckbox().isChecked());
        assertEnabled(getProject1(), getProject1Repository1(), true);

        // Disable it
        page.getAutoUnapproveCheckbox().uncheck();
        page.save();

        // Visit the page and ensure its disabled
        page = visitPullRequestCreatePage();
        assertFalse(page.getAutoUnapproveCheckbox().isChecked());
        assertEnabled(getProject1(), getProject1Repository1(), false);
    }

    private static PullRequestRepositorySettingsPage visitPullRequestCreatePage() {
        return BITBUCKET.visit(PullRequestRepositorySettingsPage.class, getProject1(), getProject1Repository1());
    }
}
