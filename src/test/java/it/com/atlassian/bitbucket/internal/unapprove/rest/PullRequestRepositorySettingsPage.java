package it.com.atlassian.bitbucket.internal.unapprove.rest;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverCheckboxElement;
import com.atlassian.webdriver.bitbucket.page.BaseRepositoryPage;

public class PullRequestRepositorySettingsPage extends BaseRepositoryPage {

    @ElementBy(id = "autoUnapprove")
    private WebDriverCheckboxElement autoUnapprove;
    @ElementBy(cssSelector = "form.pull-request-settings #submit")
    private PageElement saveButton;

    public PullRequestRepositorySettingsPage(String projectKey, String slug) {
        super(projectKey, slug);
    }

    public WebDriverCheckboxElement getAutoUnapproveCheckbox() {
        return autoUnapprove;
    }

    @Override
    public String getUrl() {
        return String.format("/projects/%s/repos/%s/settings/pull-requests", projectKey, slug);
    }

    public PullRequestRepositorySettingsPage save() {
        scrollToBottom();
        waitForPageLoad(saveButton::click);
        return pageBinder.bind(PullRequestRepositorySettingsPage.class, projectKey, slug);
    }
}
