package it.com.atlassian.bitbucket.internal.unapprove.ui;

import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import io.restassured.RestAssured;
import org.junit.Test;

import static com.atlassian.bitbucket.test.DefaultFuncTestData.*;
import static org.hamcrest.Matchers.containsString;

public class AutoUnapproveFormFragmentTest extends BaseRetryingFuncTest {

    // This is just a simple check to ensure the Soy template renders since there are no Selenium tests that actually
    // test the functionality of the UI form
    @Test
    public void testSoyTemplateRenders() {
        String url = getRepositoryURL(getProject1(), getProject1Repository1()) + "/settings/pull-requests";

        RestAssured.given()
                    .auth().preemptive()
                        .basic(getAdminUser(), getAdminPassword())
                .when()
                    .get(url)
                .then()
                    .log().ifValidationFails()
                    .statusCode(200)
                    .body(containsString("Unapprove automatically on new changes"))
                    .body(containsString("Reviewers approvals will be removed if new commits are pushed or the pull request is retargeted to a different branch"));
    }
}
